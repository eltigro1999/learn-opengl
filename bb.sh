#!/bin/bash

if [[ -d ./build ]]
then
	echo Here
	rm -rf build
	mkdir build
else
	mkdir build
fi

cd build
cmake ..
cmake --build .
