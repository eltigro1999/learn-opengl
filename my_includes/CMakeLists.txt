cmake_minimum_required(VERSION 3.6)

set(MY_SOURCES ./src/Shader.cpp)
set(MY_HEADERS ./headers/Shader.h)
add_library(my_libs STATIC ${MY_HEADERS} ${MY_SOURCES})
target_include_directories(my_libs PUBLIC ./headers)

#set(GLAD_PATH ./external_libs/glad)
#add_subdirectory(${GLAD_PATH})
target_link_libraries(my_libs glad)
