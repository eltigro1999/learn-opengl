
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/eltigro/development/c++/OpenGL/lectures_32BIT_Academy/first/main.cpp" "CMakeFiles/main.dir/main.cpp.o" "gcc" "CMakeFiles/main.dir/main.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eltigro/development/c++/OpenGL/lectures_32BIT_Academy/first/build/libs/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  "/home/eltigro/development/c++/OpenGL/lectures_32BIT_Academy/first/build/libs/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/eltigro/development/c++/OpenGL/lectures_32BIT_Academy/first/build/my_includes/CMakeFiles/my_libs.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
