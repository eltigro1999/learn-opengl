#include "Shader.h"

unsigned int Shader::ID() {
    return program
}

Shader::Shader (const char* vertexPath, const char* fragmentPath) {
    const char* vShaderCode;
    const char* fShaderCode;

    std::string vTempString;
    std::string fTempString;

    std::ifstream vShaderFile;
    std::ifstream fShaderFile;

    vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try{
        std::istringstream vShaderStream;
        vShaderFile.open(vertexPath);
        vShaderStream<<vShaderFile.rdbuf();
        vShaderFile.close();
        vTempString = vShaderStream.str();
        vShaderCode = vTempString.c_str();

        std::istringstream fShaderStream;
        fShaderFile.open(fragmentPath);
        fShaderStream<<fShaderFile.rdbuf();
        fShaderFile.close();
        fTempString = fShaderStream.str();
        fShaderCode - fTempString.c_str();
    }
    catch(std::ifstream::failure& e) {
        std::cout<<"ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ"<<std::endl;
    }

    unsigned int vertex;
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    checCompileErrors(vertex, "VERTEX");

    unsigned int fragment;
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &vShaderCode, NULL);
    glCompileShader(fragment);
    checCompileErrors(fragment, "FRAGMENT");

    programId= glCreateProgram();
    glAttachShader(programId, vertex);
    glAttachShader(programId, fragment);
    glLinkProgram(programID);
}