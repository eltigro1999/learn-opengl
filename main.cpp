#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <string.h>
#include "Shader.h"

void OnResize(GLFWwindow *win, int width, int height);
void processInput(GLFWwindow *win);

struct Color
{
	float r, g, b, a;
};

Color background = {0.f, 0.f, 0.f, 0.f};

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow *win = glfwCreateWindow(500, 500, "OpenGL Window", nullptr, nullptr);

	if (win == nullptr)
	{
		std::cout << "Error. Couldn't create window!" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(win);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Error. Couldn't load glad!" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwSetFramebufferSizeCallback(win, OnResize);

	glViewport(0, 0, 500, 500);

	const int verts = 4;

	float polygon[verts * 6] = {
		.0f, .75f, .0f, 1.f, .0f, 1.f,
		.5f, .0f, .0f, 1.f, 0.f, 0.f,
		-.5f, .0f, .0f, .0f, 1.f, .0f,
		.0f, -.75f, .0f, .0f, .0f, 1.f};

	unsigned int indices[] = { 0,
							   1,
							   2,
							   1,
							   2,
							   3 };

	unsigned int VBO_polygon;
	unsigned int VAO_polygon;
	unsigned int EBO_polygon;
	glGenBuffers(1, &VBO_polygon);
	glGenBuffers(1, &EBO_polygon);

	glGenVertexArrays(1, &VAO_polygon);

	glBindVertexArray(VAO_polygon);

	glBindBuffer(GL_ARRAY_BUFFER, VBO_polygon);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * verts * 6, polygon, GL_STATIC_DRAW); // loads data (polygon variable) into graphic card
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_polygon);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*6, indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	Shader* polygon_shader = new Shader("../Shaders/basic.vs", "../Shaders/basic.fs");

	float color[] = {1.f, 0.f, 0.f};

	while (!glfwWindowShouldClose(win))
	{
		processInput(win);

		glClearColor(background.r, background.g, background.b, background.a);
		glClear(GL_COLOR_BUFFER_BIT);

		polygon_shader->use();
		polygon_shader->setFloatVec("uniformColor", color, 3);
		glBindVertexArray(VAO_polygon);
		// glDrawArrays(GL_TRIANGLES, 0, verts);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(win);
		glfwPollEvents();
	}
	delete polygon_shader;	
	glfwTerminate();
	return 0;
}

void OnResize(GLFWwindow *win, int width, int height)
{
	glViewport(0, 0, width, height);
	std::cout << "Resized to " << width << "x" << height << std::endl;
}

void processInput(GLFWwindow *win)
{
	if (glfwGetKey(win, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(win, true);
	}
	if (glfwGetKey(win, GLFW_KEY_1) == GLFW_PRESS)
	{
		background = {1.f, 0.f, 0.f, 1.f};
	}
	else if (glfwGetKey(win, GLFW_KEY_2) == GLFW_PRESS)
	{
		background = {0.f, 1.f, 0.f, 1.f};
	}
	else if (glfwGetKey(win, GLFW_KEY_3) == GLFW_PRESS)
	{
		background = {0.f, 0.f, 1.f, 1.f};
	}
}
